package com.mycompany.week2;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nine
 */
public class Xo {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row = 1, col = 1;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;

    public static void main(String[] args) {

        showWelcome();
        while(true){
        showTable();
        showTurn();
        inputRowCol();
        process();
        if(finish){
            break;
        }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {

        System.out.print("Please input row,col : ");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showTable();
                System.out.println(">>> " + currentPlayer + " Win<<<");
                return;
            }
            switchPlayer();
        }

    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;

    }

    private static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r - 1][col] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row][c - 1] != currentPlayer) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for(int i=0;i<table.length;i++){
            if (table[i][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for(int i=0;i<table.length;i++){
            if (table[i][2-i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
}

